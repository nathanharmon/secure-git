# Secure Git

Setup PGP commit verification using the private email of Git web services.

Each Git web service gives a user their own private email address. Since the email is different between services, the user must setup of multiple PGP keys for each service.

## Requirements
- GnuPG
- Git
- An account at GitHub and or GitLab
- A place to store a password for your GPG keys

## Setup

#### Get Private Email Address
**GitHub**
- Go to Settings > Emails > Keep my email addresses private. Check this and "Block command line pushes..."
- Take note of the private email in the subsection.

**GitLab**
- Go to Preferences > Profile > Commit Email
- Set the commit email to the private email and take note of this email.

#### Create GPG Keys
- `$ gpg --full-gen-key`
- Please select what kind of key you want: **(1) RSA and RSA**
- What keysize do you want? **4096**
- Please specify how long the key should be valid. **2y** (two years)
- Enter your name.
- Enter the private email address.
- Leave comment blank.
- Confirm and press (O), then type in a secure password.
- Setup a calendar event or reminder for slightly less than two years to extend your keys.

*Repeat this process for your other Git services.*

#### Export Public Keys
 - `$ gpg --list-keys`
 - Find the correct key for the service.
 - `$ gpg --armor --export (key fingerprint)`
 - Go to the GPG key section, and add a key.
 - Paste in the output of the export command.

 Do this for all Git services you are using.

#### Use the Script
 - Download pyyaml `$ pip3 install pyyaml`
 - Clone the repo: `$ git clone https://gitlab.com/nathanharmon/secure-git.git ~/.config/`
 - Edit config.yaml to include the private email of each of the services.
 - Move setuprepo to ~/.local/bin: `$ mv ~/.config/secure-git/setuprepo ~/.local/bin/`
 - At the "key" section in config.yaml, enter the GPG key fingerprint for that service.

## Usage

 **Create your repo like normal.**

#### Run the Script
 - Navigate to your repo then: `$ setuprepo <service name>`

 Do this for each new repo.

 - When committing, ensure that you use the "-S" option. Ex. `$ git commit -S -m "This is a secure commit."`

## License

 Copyright (c) 2021 Nathan Harmon

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
